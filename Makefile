CC = mpic++
CXXFLAGS = -std=c++17 -Wpedantic -Werror -Wall
LDLIBS = -lmpi
LDFLAGS =

OBJ = algorep.o memory.o internalNode.o utils.o
VPATH = src
BIN = algorep

$(BIN): $(OBJ)

run: $(BIN)
	LD_LIBRARY_PATH=/usr/local/lib mpirun -np 11 -hostfile hfile ./$^

clean:
	$(RM) $(OBJ)
