#include "utils.hh"

bool operator<(const timestamp& ts1, const timestamp& ts2)
{
  return ts1.tv_sec == ts2.tv_sec ? ts1.tv_usec < ts2.tv_usec : ts1.tv_sec < ts2.tv_sec;
}
