#include <cmath>
#include <iostream>
#include <vector>

#include "mpi.h"

#include "internalNode.hh"
#include "memory.hh"

int main(int argc, char** argv)
{
  const int grid_size = 9;
  MPI_Init(&argc, &argv);

  int world_rank;
  MPI_Comm_rank(MPI_COMM_WORLD, &world_rank);

  int world_size;
  MPI_Comm_size(MPI_COMM_WORLD, &world_size);

  const int p = std::sqrt(grid_size);

  if (world_rank == world_size - 1)
  {
    std::cout << "User: " << world_rank << "\n";
    std::vector<int> reachable = { 0, 8 };
    Memory memory(world_rank, world_size, reachable);

    // Try alloc, read, write single variable.
    int id = memory.alloc(42);
    bool success;
    int val = memory.read(id, success);
    std::cout << "User: " << world_rank << " id:" << id << " read:" << val << "\n";

    memory.write(id, 84);
    val = memory.read(id, success);
    std::cout << "User: " << world_rank << " id:" << id << " read:" << val << "\n";

    memory.free(id);

    // Try to allocate an array that can't be allocated
    memory.m_alloc(100000);

    std::cout << "User: " << world_rank << ": END\n";
  }
  else if (world_rank == world_size - 2)
  {
    std::cout << "USER: " << world_rank << "\n";
    std::vector<int> reachable = { 0, 8 };
    Memory memory(world_rank, world_size, reachable);

    // Try alloc, read, write single variable.
    int id = memory.alloc(21);
    bool success;
    int val = memory.read(id, success);
    std::cout << "User: " << world_rank << " id:" << id << " read:" << val << "\n";

    memory.write(id, 42);
    val = memory.read(id, success);
    std::cout << "User: " << world_rank << " id:" << id << " read:" << val << "\n";

    memory.free(id);

    // Try alloc, write, read on array.
    id = memory.m_alloc(400);
    auto wdata = std::vector<int>(70);
    for (int i = 0; i < 70; ++i)
      wdata[i] = i;
    success = memory.m_write(id, wdata, 50);

    auto data = memory.m_read(id, 50, 70);
    for (int i = 0; i < 70; ++i)
      std::cout << data[i] << "\n";
    memory.free(id);

    std::cout << "User: " << world_rank << ": END\n";
  }
  else if (world_rank < grid_size)
  {
    // Launch the grid
    InternalNode node(world_rank, p);
    node.run();
  }

  MPI_Finalize();
}
