#include "internalNode.hh"

#include <iostream>

#include "mpi.h"

InternalNode::InternalNode(const int id, const int p, bool running)
  : _id(id)
  , _row(id / p)
  , _col(id % p)
  , _p(p)
  , _running(running)
  , _less_filled(id)
  , _less_filled_size(MAX_SIZE)
  , _last_id(0)
  , _counter(0)
  , _grant()
  , _inquire()
  , _yield()
  , _r_size(MAX_SIZE)
  , _s_memory()
  , _m_memory()
  , _locks()
{ }

void InternalNode::run()
{
  while (_running)
  {
    _wait_msg();
  }
}


/*-- Sending ----------------------------------------------------------------*/

void InternalNode::_send_quorum(const std::vector<int>& msg, const msg_t tag) const
{
  for (int i = 0; i < _p; ++i)
  {
    if (i != _row)
    {
      _send_to(msg, tag, i * _p + _col);
    }
    if (i != _col)
    {
      _send_to(msg, tag, _row * _p + i);
    }
  }
}

void InternalNode::_send_to(const std::vector<int>& msg, const msg_t tag, const int i_dst) const
{
  MPI_Send(&msg[0], msg.size(), MPI_INT, i_dst, underlying(tag), MPI_COMM_WORLD);
}

/*-- Receiver ---------------------------------------------------------------*/

void InternalNode::_wait_msg()
{
  // Wait for a new message
  MPI_Status status;
  MPI_Probe(MPI_ANY_SOURCE, MPI_ANY_TAG, MPI_COMM_WORLD, &status);

  const int i_src = status.MPI_SOURCE;
  const msg_t tag = static_cast<msg_t>(status.MPI_TAG);
  int l_msg = 0;
  MPI_Get_count(&status, MPI_INT, &l_msg);

  // Read the message
  std::vector<int> msg(l_msg);
  MPI_Recv(&msg[0], l_msg, MPI_INT, i_src, underlying(tag), MPI_COMM_WORLD, MPI_STATUS_IGNORE);

  switch (tag)
  {
    // Msg to lock
    case msg_t::FAIL:    _recv_failed(i_src, msg);  break;
    case msg_t::GRANT:   _recv_grant(i_src, msg);   break;
    case msg_t::INQUIRE: _recv_inquire(i_src, msg); break;
    case msg_t::RELEASE: _recv_release(i_src, msg); break;
    case msg_t::REQUIRE: _recv_require(i_src, msg); break;
    case msg_t::YIELD:   _recv_yield(i_src, msg);   break;
    // Msg for user interface
    case msg_t::KILL:    _recv_kill(i_src, msg);    break;
    case msg_t::LAST_ID: _recv_last_id(i_src, msg); break;
    case msg_t::SEARCH:  _recv_search(i_src, msg);  break;
    // Msg for single data
    case msg_t::S_ALLOC: _recv_s_alloc(i_src, msg); break;
    case msg_t::S_READ:  _recv_s_read(i_src, msg);  break;
    case msg_t::S_WRITE: _recv_s_write(i_src, msg); break;
    case msg_t::S_FREE: _recv_s_free(i_src, msg);   break;
    // Msg for multiple data
    case msg_t::M_ALLOC: _recv_m_alloc(i_src, msg); break;
    case msg_t::M_READ:  _recv_m_read(i_src, msg);  break;
    case msg_t::M_WRITE: _recv_m_write(i_src, msg); break;
    case msg_t::M_FREE:  _recv_m_free(i_src, msg);  break;
  }
}

/*-- Lock Management --------------------------------------------------------*/

/* Function to handle a FAIL message */
void InternalNode::_recv_failed(const int i_src, const std::vector<int>& msg)
{
  const int v_id = msg[ID_IDX];
  if (_grant.find(v_id) == _grant.cend())
  {
    // Create grant tuple: { granted, failed }
    _grant[v_id] = { 0, 0 };
  }
  // Update Failed counter
  auto& t = _grant.at(v_id);
  std::get<1>(t) += 1;

  if (msg[TAG_IDX] == 0)
  {
    // Notify the user that require has been denied
    const int n_id = msg[CRT_IDX];
    auto n_msg = msg;
    _send_to(n_msg, msg_t::FAIL, n_id);
    _send_quorum(n_msg, msg_t::RELEASE);
  }
  else
  {
    // Send the potential yield waiting
    const auto it = _inquire.find(v_id);
    if (it != _inquire.cend())
    {
      auto q = it->second;
      while (q.size() > 0)
      {
        auto n_msg = msg;
        n_msg[TAG_IDX] = 1;
        _send_to(n_msg, msg_t::YIELD, q.front());
        q.pop();
      }
      _inquire.erase(it);
    }
  }
}

/* Function to handle a GRANT message */
void InternalNode::_recv_grant(const int i_src, const std::vector<int>& msg)
{
  const int v_id = msg[ID_IDX];
  if (_grant.find(v_id) == _grant.cend())
  {
    // Create grant tuple: { granted, failed }
    _grant[v_id] = { 0, 0 };
  }
  // Update Granted counter
  auto& t = _grant.at(v_id);
  std::get<0>(t) += 1;

  // Check if has a grant response from all the quorm
  if (std::get<0>(t) == 2 * (_p - 1))
  {
    std::get<1>(t) = 0;
    auto it = _inquire.find(v_id);
    if (it != _inquire.cend())
    {
      // Send the potential yield waiting
      auto& q = it->second;
      while (q.size() > 0)
      {
        auto n_msg = msg;
        n_msg[TAG_IDX] = 0;
        _send_to(n_msg, msg_t::YIELD, q.front());
        q.pop();
      }
      _inquire.erase(it);
    }

    // Notify the user that require has been granted by all the quorum
    const int n_id = msg[CRT_IDX];
    auto n_msg = msg;
    _send_to(n_msg, msg_t::GRANT, n_id);
  }
}

/* Function to handle a INQUIRE message */
void InternalNode::_recv_inquire(const int i_src, const std::vector<int>& msg)
{
  const int v_id = msg[ID_IDX];
  if (_grant.find(v_id) == _grant.cend())
  {
    // Create grant tuple: { granted, failed }
    _grant[v_id] = { 0, 0 };
  }
  // Get the failed counter
  const int n_failed = std::get<1>(_grant.at(v_id));

  if (n_failed > 0)
  {
    // If has receive a FAILED, send positive yield
    auto n_msg = msg;
    n_msg[TAG_IDX] = 1;
    _send_to(n_msg, msg_t::YIELD, i_src);
  }
  else
  {
    // If no msg FAIL receive: add ID in inquire queue (yield will be send when
    // a FAILED will be receive or all the grant will be receive.
    auto it = _inquire.find(v_id);
    if (it == _inquire.cend())
    {
      std::queue<int> q;
      q.push(i_src);
      _inquire[v_id] = q;
    }
    else
    {
      it->second.push(v_id);
    }
  }
}

/* Function to handle a RELEASE message */
void InternalNode::_recv_release(const int i_src, const std::vector<int>& msg)
{
  const int v_id = msg[ID_IDX];
  auto it = _locks.find(v_id);

  if (it == _locks.cend())
  {
    // Quit if the lock does not exist.
    return;
  }

  auto& list = it->second;
  if (std::get<0>(list.front()) != i_src)
  {
    // Quit if the owner of the lock is not the src
    return;
  }
  // Remove lock
  list.pop_front();

  if (!_is_internalNode(i_src))
  {
    // Send to quorum if the msg come from a user.
    auto n_msg = msg;
    _send_quorum(n_msg, msg_t::RELEASE);
  }

  if (list.size() > 0)
  {
    // Send grant to the node waiting for the lock
    const auto& t = list.front();
    const int n_id = std::get<0>(t);
    auto n_msg = to_int(std::get<1>(t));
    n_msg.push_back(std::get<2>(t));
    n_msg.push_back(v_id);
    n_msg.push_back(0);

    if (_is_internalNode(n_id))
    {
      // If the waiting owner is an internal node: send GRANT.
      _send_to(n_msg, msg_t::GRANT, std::get<0>(t));
    }
    else
    {
      // If the waiting owner is an owner: send REQUIRE to the quorum.
      // and clean the previous grant counter
      auto g_it = _grant.find(v_id);
      if (g_it != _grant.cend())
      {
        _grant.erase(g_it);
      }
      _send_quorum(n_msg, msg_t::REQUIRE);
    }
  }
  else
  {
    // If no one wait: just clean lock and counter
    _locks.erase(it);
    auto g_it = _grant.find(v_id);
    if (g_it != _grant.cend())
    {
      _grant.erase(g_it);
    }
  }
}

/* Function to handle a REQUIRE message */
void InternalNode::_recv_require(const int i_src, const std::vector<int>& msg)
{
  const auto ts = to_timestamp(msg);
  const int v_id = msg[ID_IDX];
  auto it = _locks.find(v_id);

  if (it == _locks.cend())
  {
    // TODO: can check if it own the variable and directly send FAIL 0 if the
    // timestamp is in the past.
    auto t = std::make_tuple(i_src, ts, msg[CRT_IDX]);
    _locks[v_id] = { t };
    if (_is_internalNode(i_src))
    {
      // If internal node is asking give GRANT
      _send_to(msg, msg_t::GRANT, i_src);
    }
    else
    {
      // If user is asking: send REQUIRE to the quorum
      _send_quorum(msg, msg_t::REQUIRE);
    }
  }
  else
  {
    // If there is already a lock
    auto& list = it->second;
    const auto& t = list.front();
    if (msg[TAG_IDX] == 0 && std::get<1>(t) < ts)
    {
      // Had to queue and send FAIL if an internal node asked.
      _locks[v_id].push_back({ i_src, ts, msg[CRT_IDX] });
      auto n_msg = msg;
      n_msg[TAG_IDX] = 1;
      if (_is_internalNode(i_src))
      {
        _send_to(n_msg, msg_t::FAIL, i_src);
      }
    }
    else
    {
      // If timestamp is older than current and its a readlock: send INQUIRE
      int dst = std::get<0>(t);
      dst = _is_internalNode(dst) ? dst : _id;
      _send_to(msg, msg_t::INQUIRE, dst);
      _yield[v_id].push({ i_src, ts , msg[CRT_IDX] });
    }
  }
}

/* Function to handle a YIELD message */
void InternalNode::_recv_yield(const int i_src, const std::vector<int>& msg)
{
  const int v_id = msg[ID_IDX];
  auto it = _yield.find(v_id);
  if (it == _yield.cend())
  {
    std::cerr << "ERROR: Node " << _id << ": receive yield on ghost.\n";
    return;
  }

  if (msg[TAG_IDX] == 0)
  {
    // Yield refused: send fail to waiting nodes
    auto& q = it->second;
    while (q.size() > 0)
    {
      auto n_msg = msg;
      n_msg[TAG_IDX] = 0;
      _send_to(n_msg, msg_t::FAIL, std::get<0>(q.front()));
      q.pop();
    }
  }
  else
  {
    // Yield success
    auto l_it = _locks.find(v_id);
    if (l_it == _locks.end())
    {
      std::cerr << "ERROR: Node " << _id << ": receive yield on non-lock.\n";
      return;
    }

    // Insert the waiting node in order chronological order
    auto& q = it->second;
    auto& list = l_it->second;
    while (q.size() > 0)
    {
      const auto& ts = std::get<1>(q.front());
      auto l = list.begin();
      for (; l != list.end() && std::get<1>(*l) < ts; ++l)
        continue;

      if (l == list.end())
      {
        std::cerr << "ERROR: Node " << _id << ": receive yield on non-lock or in future.\n";
        return;
      }
      list.insert(l, q.front());
      q.pop();
    }

    const auto& t = list.front();
    auto n_msg = to_int(std::get<1>(t));
    n_msg.push_back(std::get<2>(t));
    n_msg.push_back(v_id);
    n_msg.push_back(0);
    if (_is_internalNode(std::get<0>(t)))
    {
      _send_to(n_msg, msg_t::GRANT, std::get<0>(t));
    }
    else
    {
      // Special case: the INQUIRE was send for a user.
      _send_quorum(n_msg, msg_t::REQUIRE);
    }
  }
}

/*-- User interface ---------------------------------------------------------*/

/* Function to handle a KILL message
 *
 * Simply send KILL to all the quorum and stop running
 */
void InternalNode::_recv_kill(const int i_src, const std::vector<int>& msg)
{
  if (_is_internalNode(i_src))
  {
    _send_quorum(msg, msg_t::KILL);
    _running = false;
  }
  else
  {
    const auto it = _locks.find(msg[ID_IDX]);
    if (it == _locks.cend() || std::get<0>(it->second.front()) != i_src)
    {
      std::cerr << "ERROR: Node " << _id << ": receive kill but not lock or not owner.\n";
      return;
    }
    _send_quorum(msg, msg_t::KILL);
    _running = false;
  }
}

/* Function to handle a LAST_ID message
 *
 * Function used to find a new ID for allocation and also found the lesss filled
 * node (the one with more free space).
 */
void InternalNode::_recv_last_id(const int i_src, const std::vector<int>& msg)
{
  if (_is_internalNode(i_src))
  {
    if (msg[TAG_IDX] == 0)
    {
      // Resend the information: last_id and free space
      auto n_msg = msg;
      n_msg[TAG_IDX] = 1;
      n_msg.push_back(_last_id);
      n_msg.push_back(_r_size);
      _send_to(n_msg, msg_t::LAST_ID, i_src);
    }
    else if (msg[TAG_IDX] == 1)
    {
      // Update info from other nodes
      _counter += 1;
      _last_id = _last_id < msg[DATA_IDX] ? msg[DATA_IDX] : _last_id;
      if (_less_filled_size < msg[DATA_IDX + 1])
      {
        _less_filled = i_src;
        _less_filled_size = msg[DATA_IDX + 1];
      }

      if (_counter == 2 * (_p - 1))
      {
        // Send respones to the user if it get all the responses and send new
        // last_id to quorum
        _last_id += 1;
        _counter = 0;

        auto n_msg = msg;
        n_msg[TAG_IDX] = 2;
        n_msg[DATA_IDX] = _last_id;
        n_msg[DATA_IDX + 1] = _less_filled;

        _less_filled = _id;
        _less_filled_size = _r_size;

        _send_to(n_msg, msg_t::LAST_ID, n_msg[CRT_IDX]);
        _send_quorum(n_msg, msg_t::LAST_ID);
      }
    }
    else
    {
      // Update last_id
      _last_id = _last_id < msg[DATA_IDX] ? msg[DATA_IDX] : _last_id;
    }
  }
  else
  {
    // Ask last id and free space to the quorum
    _send_quorum(msg, msg_t::LAST_ID);
  }
}

/* Function to handele SEARCH message:
 *
 * Used to get the node with the more free space
 * Barely the same than _recv_last_id
 */
void InternalNode::_recv_search(const int i_src, const std::vector<int>& msg)
{
  if (_is_internalNode(i_src))
  {
    if (msg[TAG_IDX] == 0)
    {
      // Send free space
      auto n_msg = msg;
      n_msg[TAG_IDX] = 1;
      n_msg.push_back(_r_size);
      _send_to(n_msg, msg_t::SEARCH, i_src);
    }
    else if (msg[TAG_IDX] == 1)
    {
      // Update from response
      _counter += 1;
      if (_less_filled_size < msg[DATA_IDX])
      {
        _less_filled = i_src;
        _less_filled_size = msg[DATA_IDX];
      }

      if (_counter == 2 * (_p - 1))
      {
        // If all quormu answer, send response to the user
        _counter = 0;
        auto n_msg = msg;
        n_msg[DATA_IDX] = _less_filled;
        _less_filled = _id;
        _less_filled_size = _r_size;
        _send_to(n_msg, msg_t::SEARCH, n_msg[CRT_IDX]);
      }
    }
  }
  else
  {
    // Ask free space to all the quorum
    _send_quorum(msg, msg_t::SEARCH);
  }
}

/*-- Single data action -----------------------------------------------------*/

/* Function to handle a S_ALLOC message:
 *
 * Simply transfer the message the correct node that will alloctate the new
 * variable.
 */
void InternalNode::_recv_s_alloc(const int i_src, const std::vector<int>& msg)
{
  const int v_id = msg[ID_IDX];
  const int n_src = msg[DATA_IDX];
  const int n_dst = msg[DATA_IDX + 1];

  if (_id == n_dst)
  {
    // If it the destination node: allocate and send if it success or not.
    auto n_msg = msg;
    auto it = _s_memory.find(v_id);
    const auto ret = _is_internalNode(i_src) ? _get_intermediate(n_src) : i_src;
    if (it == _s_memory.cend())
    {
      _s_memory[v_id] = { msg[DATA_IDX + 2], to_timestamp(msg) };
      --_r_size;
      n_msg[DATA_IDX + 2] = 1;
    }
    else
    {
      n_msg[DATA_IDX + 2] = 0;
    }
    _send_to(n_msg, msg_t::S_ALLOC, ret);
  }
  else if (_id == n_src)
  {
    // Get the response and send it the user or send to the transition node.
    const auto ret = _is_internalNode(i_src) ? msg[CRT_IDX] : _get_intermediate(n_dst);
    _send_to(msg, msg_t::S_ALLOC, ret);
  }
  else
  {
    // Transition node: send to the correct one..
    const auto ret = i_src == n_src ? n_dst : n_src;
    _send_to(msg, msg_t::S_ALLOC, ret);
  }
}

/* Function to handle a S_ALLOC message:
 *
 * Simply transfer the message the correct node that will resend the value asked.
 */
void InternalNode::_recv_s_read(const int i_src, const std::vector<int>& msg)
{
  const int v_id = msg[ID_IDX];
  const int n_src = msg[DATA_IDX];
  const int n_dst = msg[DATA_IDX + 1];

  if (_id == n_dst)
  {
    // It's the destination node: send back the value.
    auto n_msg = msg;
    auto it = _s_memory.find(v_id);
    const auto ret = _is_internalNode(i_src) ? _get_intermediate(n_src) : i_src;
    if (it != _s_memory.cend())
    {
      n_msg.push_back(std::get<0>(it->second));
      n_msg[TAG_IDX] = 1;
    }
    else
    {
      n_msg[TAG_IDX] = 0;
    }
    _send_to(n_msg, msg_t::S_READ, ret);
  }
  else if (_id == n_src)
  {
    // Get the response and send it the user or send to the transition node.
    const auto ret = _is_internalNode(i_src) ? msg[CRT_IDX] : _get_intermediate(n_dst);
    _send_to(msg, msg_t::S_READ, ret);
  }
  else
  {
    // Transition node: send to the correct one..
    const auto ret = i_src == n_src ? n_dst : n_src;
    _send_to(msg, msg_t::S_READ, ret);
  }
}

/* Function to handle a S_WRITE message:
 *
 * Simply transfer the message the correct node that will write the asked value.
 */
void InternalNode::_recv_s_write(const int i_src, const std::vector<int>& msg)
{
  const int v_id = msg[ID_IDX];
  const int n_src = msg[DATA_IDX];
  const int n_dst = msg[DATA_IDX + 1];

  if (_id == n_dst)
  {
    // It's the destination node: wirte the value and send if succeed or not.
    auto n_msg = msg;
    auto it = _s_memory.find(v_id);
    const auto ret = _is_internalNode(i_src) ? _get_intermediate(n_src) : i_src;
    const auto ts = to_timestamp(msg);
    if (it != _s_memory.cend() && std::get<1>(it->second) < ts)
    {
      it->second = { msg[DATA_IDX + 2], ts };
      n_msg[TAG_IDX] = 1;
    }
    else
    {
      n_msg[TAG_IDX] = 0;
    }
    _send_to(n_msg, msg_t::S_WRITE, ret);
  }
  else if (_id == n_src)
  {
    // Get the response and send it the user or send to the transition node.
    const auto ret = _is_internalNode(i_src) ? msg[CRT_IDX] : _get_intermediate(n_dst);
    _send_to(msg, msg_t::S_WRITE, ret);
  }
  else
  {
    // Transition node: send to the correct one..
    const auto ret = i_src == n_src ? n_dst : n_src;
    _send_to(msg, msg_t::S_WRITE, ret);
  }
}

/* Function to handle a S_FREE message:
 *
 * Simply transfer the message the correct node that will free the value if it's
 * not used by someone else..
 */
void InternalNode::_recv_s_free(const int i_src, const std::vector<int>& msg)
{
  const int v_id = msg[ID_IDX];
  const int n_src = msg[DATA_IDX];
  const int n_dst = msg[DATA_IDX + 1];

  if (_id == n_dst)
  {
    // It's the destination node: try to free
    auto it = _s_memory.find(v_id);
    const auto ts = to_timestamp(msg);
    if (std::get<1>(it->second) < ts)
    {
      // If another process still use it the value is not free.
      _s_memory.erase(it);
    }
  }
  else if (_id == n_src)
  {
    // Get the response and send it the user or send to the transition node.
    _send_to(msg, msg_t::S_FREE, _get_intermediate(n_dst));
  }
  else
  {
    // Transition node: send to the correct one..
    _send_to(msg, msg_t::S_FREE, n_dst);
  }
}

/*-- Multiple data action ---------------------------------------------------*/

/* Function to handle a M_ALLOC message:
 *
 * Simply transfer the message the correct node that will try to allocated. If
 * not enougth memory, send to length missing.
 */
void InternalNode::_recv_m_alloc(const int i_src, const std::vector<int>& msg)
{
  const int v_id = msg[ID_IDX];
  const int n_src = msg[N_SRC];
  const int n_dst = msg[N_DST];

  if (_id == n_dst)
  {
    // It's the destination node: try allocate the need array and send if succeed
    // or not.
    auto n_msg = msg;
    auto it = _m_memory.find(v_id);
    const auto ret = _is_internalNode(i_src) ? _get_intermediate(n_src) : i_src;
    if (it == _m_memory.cend() && _r_size != 0)
    {
      const auto len = _r_size < (unsigned)msg[M_LEN] ? _r_size : msg[M_LEN];
      _m_memory[v_id] = { msg[M_START], std::vector<int>(len), to_timestamp(msg) };
      _r_size -= len;
      n_msg[TAG_IDX] = 1;
      n_msg[M_START] = msg[M_START] + len;
      n_msg[M_LEN] = msg[M_LEN] - len;
    }
    else
    {
      n_msg[TAG_IDX] = 0;
    }
    _send_to(n_msg, msg_t::M_ALLOC, ret);
  }
  else if (_id == n_src)
  {
    // Get the response and send it the user or send to the transition node.
    const auto ret = _is_internalNode(i_src) ? msg[CRT_IDX] : _get_intermediate(n_dst);
    _send_to(msg, msg_t::M_ALLOC, ret);
  }
  else
  {
    // Transition node: send to the correct one..
    const auto ret = i_src == n_src ? n_dst : n_src;
    _send_to(msg, msg_t::M_ALLOC, ret);
  }
}

/* Function to handle a M_READ message:
 *
 * Simply transfer the message the correct node that will resend the value asked.
 */
void InternalNode::_recv_m_read(const int i_src, const std::vector<int>& msg)
{
  const int v_id = msg[ID_IDX];
  const int n_src = msg[N_SRC];
  const int n_dst = msg[N_DST];

  if (_id == n_dst)
  {
    // It's the destination node: try to read the array and send if succeed or
    // not.
    auto n_msg = msg;
    auto it = _m_memory.find(v_id);
    const auto ret = _is_internalNode(i_src) ? _get_intermediate(n_src) : i_src;
    if (it != _m_memory.cend())
    {
      const auto st = std::get<0>(it->second);
      const auto& arr = std::get<1>(it->second);

      const unsigned s = st < msg[M_START] ? msg[M_START] : st;
      unsigned len = msg[M_LEN] - (s - msg[M_START]);
      len = s - st + len > arr.size() ? arr.size() + st - s : len;
      for (unsigned i = s; i < s + len; ++i)
      {
        n_msg.push_back(arr.at(i - st));
      }
      n_msg[M_LEN] = len;
      n_msg[TAG_IDX] = 1;
    }
    else
    {
      n_msg[TAG_IDX] = 0;
    }
    _send_to(n_msg, msg_t::M_READ, ret);
  }
  else if (_id == n_src)
  {
    // Get the response and send it the user or send to the transition node.
    const auto ret = _is_internalNode(i_src) ? msg[CRT_IDX] : _get_intermediate(n_dst);
    _send_to(msg, msg_t::M_READ, ret);
  }
  else
  {
    // Transition node: send to the correct one..
    const auto ret = i_src == n_src ? n_dst : n_src;
    _send_to(msg, msg_t::M_READ, ret);
  }
}

/* Function to handle M_WRITE message:
 *
 * Simply transfer the message the correct node that will write the given
 * values.
 */
void InternalNode::_recv_m_write(const int i_src, const std::vector<int>& msg)
{
  const int v_id = msg[ID_IDX];
  const int n_src = msg[DATA_IDX];
  const int n_dst = msg[DATA_IDX + 1];

  if (_id == n_dst)
  {
    // It's the destination node: try to write the array and send if succeed or
    // not.
    auto n_msg = msg;
    auto it = _m_memory.find(v_id);
    const auto ret = _is_internalNode(i_src) ? _get_intermediate(n_src) : i_src;
    const auto ts = to_timestamp(msg);
    if (it != _m_memory.cend() && std::get<2>(it->second) < ts)
    {
      const auto st = std::get<0>(it->second);
      auto& arr = std::get<1>(it->second);

      unsigned s = st < msg[M_START] ? msg[M_START] : st;
      unsigned len = msg[M_LEN] - (s - msg[M_START]);
      len = s + len > arr.size() + st ? arr.size() + st - s : len;

      for (unsigned i = s; i < s + len; ++i)
      {
        arr[i - st] = msg[M_DATA + i - msg[M_START]] ;
      }
      n_msg[TAG_IDX] = 1;
    }
    else
    {
      n_msg[TAG_IDX] = 0;
    }
    _send_to(n_msg, msg_t::M_WRITE, ret);
  }
  else if (_id == n_src)
  {
    // Get the response and send it the user or send to the transition node.
    const auto ret = _is_internalNode(i_src) ? msg[CRT_IDX] : _get_intermediate(n_dst);
    _send_to(msg, msg_t::M_WRITE, ret);
  }
  else
  {
    // Transition node: send to the correct one..
    const auto ret = i_src == n_src ? n_dst : n_src;
    _send_to(msg, msg_t::M_WRITE, ret);
  }
}
void InternalNode::_recv_m_free(const int i_src, const std::vector<int>& msg)
{
  const int v_id = msg[ID_IDX];
  const int n_src = msg[DATA_IDX];
  const int n_dst = msg[DATA_IDX + 1];

  if (_id == n_dst)
  {
    // It's the destination node: try to free.
    auto it = _m_memory.find(v_id);
    const auto ts = to_timestamp(msg);
    if (std::get<2>(it->second) < ts)
    {
      // If another process still use it the value is not free.
      _r_size += std::get<1>(it->second).size();
      _m_memory.erase(it);
    }
  }
  else if (_id == n_src)
  {
    // Get the response and send it the user or send to the transition node.
    _send_to(msg, msg_t::M_FREE, _get_intermediate(n_dst));
  }
  else
  {
    // Transition node: send to the correct one..
    _send_to(msg, msg_t::M_FREE, n_dst);
  }
}

/*-- Misc -------------------------------------------------------------------*/

bool InternalNode::_is_internalNode(const int i_src)
{
  return i_src < _p * _p;
}

int InternalNode::_get_intermediate(const int i_dst)
{
  const int i = _p * _row + i_dst % _p;
  return i != _id ? i : (i_dst / _p) * _p + _id % _p;
}
