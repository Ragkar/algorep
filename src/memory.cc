#include <cstddef>
#include <iostream>
#include <sys/time.h>

#include "mpi.h"

#include "memory.hh"
#include "utils.hh"

Memory::Memory(int id, int n_nodes, const std::vector<int>& reachable)
  : _id(id)
  , _n_nodes(n_nodes)
  , _reachable(reachable)
  , _id_to_node()
  , _id_to_nodes()
{ }

void Memory::kill_grid() const
{
  const int i_dest = _reachable.at(0);
  while (!_ask_grant(i_dest))
    continue;

  auto msg = _create_message();
  _send_to(msg, msg_t::KILL, i_dest);
}

/*-- Single value -----------------------------------------------------------*/

int Memory::alloc(const int value)
{
  const int i_dest = 0;
  if (!_ask_grant(i_dest))
  {
    // Fail to get the lock of -1
    return -1;
  }

  auto rq = _ask_last_id(i_dest);
  bool success = _ask_s_alloc(i_dest, rq[1], rq[0], value);
  // TODO: -1 could be release earlier and rq[0] should be lock instead.
  _ask_release(i_dest);

  if (!success)
  {
    std::cerr << "MEMORY " << _id << ": faild to s_alloc.\n";
    return -1;
  }
  _id_to_node[rq[0]] = rq[1];
  return rq[0];
}

int Memory::read(int v_id, bool& success) const
{
  const int i_dest = _reachable.at(0);
  const auto it = _id_to_node.find(v_id);
  int n_id = 0;
  if (it == _id_to_node.end())
  {
    // TODO: find_id
    std::cerr << "MISSING FUNCTION: find ID.\n";
  }
  else
  {
    n_id = it->second;
  }


  if (!_ask_grant(i_dest, v_id, 1))
  {
    return false;
  }
  int val = 0;
  success = _ask_s_read(i_dest, n_id, v_id, val);
  _ask_release(i_dest, v_id);
  return val;
}

bool Memory::write(const int v_id, const int val) const
{
  const int i_dest = _reachable.at(0);
  const auto it = _id_to_node.find(v_id);
  int n_id = 0;
  if (it == _id_to_node.end())
  {
    // TODO: find_id
    std::cerr << "MISSING FUNCTION: find ID.\n";
  }
  else
  {
    n_id = it->second;
  }

  if (!_ask_grant(i_dest, v_id))
  {
    return false;
  }
  auto success = _ask_s_write(i_dest, n_id, v_id, val);
  _ask_release(i_dest, v_id);
  return success;
}

void Memory::free(const int v_id)
{
  const int i_dest = _reachable.at(0);
  const auto it = _id_to_node.find(v_id);
  int n_id = 0;
  if (it == _id_to_node.end())
  {
    // TODO: find_id
    std::cerr << "MISSING FUNCTION: find ID.\n";
  }
  else
  {
    n_id = it->second;
  }

  if (!_ask_grant(i_dest, v_id))
  {
    return;
  }
  _ask_s_free(i_dest, n_id, v_id);
  _ask_release(i_dest, v_id);
  _id_to_node.erase(it);
}

/*-- Multiple value ---------------------------------------------------------*/

int Memory::m_alloc(const int size)
{
  if (size > MAX_SIZE * _n_nodes)
  {
    return -1;
  }
  const int i_dest = 0;
  if (!_ask_grant(i_dest))
  {
    return -1;
  }

  // TODO: could also lock the ID.
  auto rq = _ask_last_id(i_dest);
  bool success = _ask_m_alloc(i_dest, rq[1], rq[0], size);
  _ask_release(i_dest);

  if (!success)
  {
    std::cerr << "MEMORY " << _id << ": faild to m_alloc.\n";
    return -1;
  }
  return rq[0];
}

std::vector<int> Memory::m_read(const int v_id, const int start, const int len) const
{
  if (len > MAX_SIZE)
  {
    return {};
  }

  const int i_dest = _reachable.at(0);

  if (!_ask_grant(i_dest, v_id, 1))
  {
    return {};
  }
  auto data = _ask_m_read(i_dest, v_id, start, len);
  _ask_release(i_dest, v_id);
  return data;
}

bool Memory::m_write(const int v_id, const std::vector<int>& data, const int start) const
{
  if (data.size() > MAX_SIZE)
  {
    return false;
  }

  const int i_dest = _reachable.at(0);

  if (!_ask_grant(i_dest, v_id))
  {
    return false;
  }
  auto success = _ask_m_write(i_dest, v_id, data, start);
  _ask_release(i_dest, v_id);
  return success;
}

void Memory::m_free(const int v_id)
{
  const int i_dest = _reachable.at(0);
  if (!_ask_grant(i_dest, v_id))
  {
    return;
  }
  _ask_m_free(i_dest, v_id);
  _ask_release(i_dest, v_id);
}

/*-- Senders ----------------------------------------------------------------*/

bool Memory::_ask_grant(const int i_dest, const int v_id, const int rw) const
{
  auto msg = _create_message(v_id);
  msg[TAG_IDX] = rw;
  _send_to(msg, msg_t::REQUIRE, i_dest);

  MPI_Status status;
  MPI_Probe(i_dest, MPI_ANY_TAG, MPI_COMM_WORLD, &status);

  const msg_t tag = static_cast<msg_t>(status.MPI_TAG);
  int l_msg = 0;
  MPI_Get_count(&status, MPI_INT, &l_msg);
  msg = std::vector<int>(l_msg);
  MPI_Recv(&msg[0], l_msg, MPI_INT, i_dest, underlying(tag), MPI_COMM_WORLD, MPI_STATUS_IGNORE);

  return tag == msg_t::GRANT;
}

void Memory::_ask_release(const int i_dest, const int v_id) const
{
  auto msg = _create_message(v_id);
  _send_to(msg, msg_t::RELEASE, i_dest);
}

/*-- Single value action ----------------------------------------------------*/

bool Memory::_ask_s_alloc(const int i_dest, const int n_dest, const int v_id, const int val) const
{
  auto msg = _create_message(v_id);
  msg.push_back(i_dest);
  msg.push_back(n_dest);
  msg.push_back(val);
  _send_to(msg, msg_t::S_ALLOC, i_dest);

  const int tag = underlying(msg_t::S_ALLOC);
  MPI_Status status;
  MPI_Probe(i_dest, tag, MPI_COMM_WORLD, &status);

  int l_msg = 0;
  MPI_Get_count(&status, MPI_INT, &l_msg);
  msg = std::vector<int>(l_msg);
  MPI_Recv(&msg[0], l_msg, MPI_INT, i_dest, tag, MPI_COMM_WORLD, MPI_STATUS_IGNORE);

  return msg[DATA_IDX + 2];
}

bool Memory::_ask_s_read(const int i_dest, const int n_dst, const int v_id, int& val) const
{
  auto msg = _create_message(v_id);
  msg.push_back(i_dest);
  msg.push_back(n_dst);
  _send_to(msg, msg_t::S_READ, i_dest);

  const int tag = underlying(msg_t::S_READ);
  MPI_Status status;
  MPI_Probe(i_dest, tag, MPI_COMM_WORLD, &status);

  int l_msg = 0;
  MPI_Get_count(&status, MPI_INT, &l_msg);
  msg = std::vector<int>(l_msg);
  MPI_Recv(&msg[0], l_msg, MPI_INT, i_dest, tag, MPI_COMM_WORLD, MPI_STATUS_IGNORE);

  val = msg[DATA_IDX + 2];
  return msg[TAG_IDX];
}

bool Memory::_ask_s_write(const int i_dest, const int n_dst, const int v_id, const int val) const
{
  auto msg = _create_message(v_id);
  msg.push_back(i_dest);
  msg.push_back(n_dst);
  msg.push_back(val);
  _send_to(msg, msg_t::S_WRITE, i_dest);

  const int tag = underlying(msg_t::S_WRITE);
  MPI_Status status;
  MPI_Probe(i_dest, tag, MPI_COMM_WORLD, &status);

  int l_msg = 0;
  MPI_Get_count(&status, MPI_INT, &l_msg);
  msg = std::vector<int>(l_msg);
  MPI_Recv(&msg[0], l_msg, MPI_INT, i_dest, tag, MPI_COMM_WORLD, MPI_STATUS_IGNORE);

  return msg[TAG_IDX];
}

void Memory::_ask_s_free(const int i_dest, const int n_dst, const int v_id) const
{
  auto msg = _create_message(v_id);
  msg.push_back(i_dest);
  msg.push_back(n_dst);
  _send_to(msg, msg_t::S_FREE, i_dest);
}

/*-- Multiple value action --------------------------------------------------*/

bool Memory::_ask_m_alloc(const int i_dest, int n_dst, const int v_id, int size)
{
  auto start = 0;
  auto n_msg = _create_message(v_id, 1);
  n_msg.push_back(i_dest);
  n_msg.push_back(n_dst);
  n_msg.push_back(start);
  n_msg.push_back(size);
  _send_to(n_msg, msg_t::M_ALLOC, i_dest);

  auto list = std::list<std::tuple<int, int ,int>>();
  const int tag = underlying(msg_t::M_ALLOC);
  while (true)
  {
    MPI_Status status;
    MPI_Probe(i_dest, tag, MPI_COMM_WORLD, &status);
    int l_msg = 0;
    MPI_Get_count(&status, MPI_INT, &l_msg);
    auto msg = std::vector<int>(l_msg);
    MPI_Recv(&msg[0], l_msg, MPI_INT, i_dest, tag, MPI_COMM_WORLD, MPI_STATUS_IGNORE);

    if (msg[TAG_IDX] == 0)
    {
      _id_to_nodes[v_id] = list;
      _ask_m_free(i_dest, v_id);
      return false;
    }

    // Get back information to fill the map
    list.push_back({ n_dst, start, size - msg[M_LEN] });
    start += size - msg[M_LEN];
    size = msg[M_LEN];
    if (size == 0)
    {
      _id_to_nodes[v_id] = list;
      return true;
    }

    // Get next node to write the next part of the array, and send.
    n_dst = _ask_search(i_dest);
    n_msg[N_DST] = n_dst;
    n_msg[M_START] = msg[M_START];
    n_msg[M_LEN] = size;
    _send_to(n_msg, msg_t::M_ALLOC, i_dest);
  }
}

std::vector<int> Memory::_ask_m_read(const int i_dest, const int v_id,
                                     const int start, const int len ) const
{
  const std::vector<int> nodes = _get_nodes(v_id, start, len);
  auto data = std::vector<int>(len);
  auto cnt = 0;

  auto n_msg = _create_message(v_id);
  n_msg.push_back(i_dest);
  n_msg.push_back(0);
  n_msg.push_back(start);
  n_msg.push_back(len);

  const int tag = underlying(msg_t::M_READ);
  for (auto it = nodes.cbegin(); it != nodes.cend(); ++it)
  {
    n_msg[N_DST] = *it;
    _send_to(n_msg, msg_t::M_READ, i_dest);

    MPI_Status status;
    MPI_Probe(i_dest, tag, MPI_COMM_WORLD, &status);
    int l_msg = 0;
    MPI_Get_count(&status, MPI_INT, &l_msg);
    auto msg = std::vector<int>(l_msg);
    MPI_Recv(&msg[0], l_msg, MPI_INT, i_dest, tag, MPI_COMM_WORLD, MPI_STATUS_IGNORE);

    if (msg[TAG_IDX] == 0)
    {
      return data;
    }

    const auto l = msg[M_LEN];
    for (auto i = 0; i < l; ++i)
    {
      data[cnt + i] = msg[M_DATA + i];
    }
    cnt += l;
  }

  return data;
}

bool Memory::_ask_m_write(const int i_dest, const int v_id, const std::vector<int>& data,
                          const int start) const
{
  bool success = true;
  const std::vector<int> nodes = _get_nodes(v_id, start, data.size());

  auto n_msg = _create_message(v_id);
  n_msg.push_back(i_dest);
  n_msg.push_back(0);
  n_msg.push_back(start);
  n_msg.push_back(data.size());
  for (unsigned i = 0; i < data.size(); ++i)
    n_msg.push_back(data[i]);

  const int tag = underlying(msg_t::M_WRITE);
  for (auto it = nodes.cbegin(); it != nodes.cend(); ++it)
  {
    n_msg[N_DST] = *it;
    _send_to(n_msg, msg_t::M_WRITE, i_dest);

    MPI_Status status;
    MPI_Probe(i_dest, tag, MPI_COMM_WORLD, &status);
    int l_msg = 0;
    MPI_Get_count(&status, MPI_INT, &l_msg);
    auto msg = std::vector<int>(l_msg);
    MPI_Recv(&msg[0], l_msg, MPI_INT, i_dest, tag, MPI_COMM_WORLD, MPI_STATUS_IGNORE);

    success = success && msg[TAG_IDX];
  }
  return success;
}

void Memory::_ask_m_free(const int i_dest, const int v_id)
{
  auto it = _id_to_nodes.find(v_id);
  if (it == _id_to_nodes.cend())
  {
    // TODO: find IDs
    std::cerr << "MISSING FUNCTION: find ID.\n";
    return;
  }

  auto msg = _create_message(v_id);
  msg.push_back(i_dest);
  msg.push_back(0);

  auto& list = it->second;
  while (list.size() > 0)
  {
    msg[N_DST] = std::get<0>(list.front());
    _send_to(msg, msg_t::S_FREE, i_dest);
    list.pop_front();
  }
  _id_to_nodes.erase(it);
}

/*-- Misc -------------------------------------------------------------------*/

std::vector<int> Memory::_ask_last_id(const int i_dest) const
{
  auto msg = _create_message();
  _send_to(msg, msg_t::LAST_ID, i_dest);

  const int tag = underlying(msg_t::LAST_ID);
  MPI_Status status;
  MPI_Probe(i_dest, tag, MPI_COMM_WORLD, &status);

  int l_msg = 0;
  MPI_Get_count(&status, MPI_INT, &l_msg);
  msg = std::vector<int>(l_msg);
  MPI_Recv(&msg[0], l_msg, MPI_INT, i_dest, tag, MPI_COMM_WORLD, MPI_STATUS_IGNORE);

  return { msg[DATA_IDX], msg[DATA_IDX + 1] };
}

int Memory::_ask_search(const int i_dest) const
{
  auto msg = _create_message();
  _send_to(msg, msg_t::SEARCH, i_dest);

  const int tag = underlying(msg_t::SEARCH);
  MPI_Status status;
  MPI_Probe(i_dest, tag, MPI_COMM_WORLD, &status);

  int l_msg = 0;
  MPI_Get_count(&status, MPI_INT, &l_msg);
  msg = std::vector<int>(l_msg);
  MPI_Recv(&msg[0], l_msg, MPI_INT, i_dest, tag, MPI_COMM_WORLD, MPI_STATUS_IGNORE);

  return msg[DATA_IDX];
}

void Memory::_send_to(const std::vector<int>& msg, const msg_t tag, const int i_dst) const
{
  MPI_Send(&msg[0], msg.size(), MPI_INT, i_dst, underlying(tag), MPI_COMM_WORLD);
}

std::vector<int> Memory::_create_message(const int v_id, const int tag) const
{
  timestamp ts;
  gettimeofday(&ts, NULL);

  auto msg = to_int(ts);
  msg.push_back(_id);
  msg.push_back(v_id);
  msg.push_back(tag);

  return msg;
}

std::vector<int> Memory::_get_nodes(const int v_id, const int start, const int len) const
{
  auto nodes = std::vector<int>();
  auto it = _id_to_nodes.find(v_id);
  if (it == _id_to_nodes.end())
  {
    // TODO: find IDs and update it
    std::cerr << "MISSING FUNCTION: find ID.\n";
    return nodes;
  }
  const auto& list = it->second;

  for (auto l_it = list.cbegin(); l_it != list.cend(); ++l_it)
  {
    const auto s = std::get<1>(*l_it);
    const auto l = std::get<2>(*l_it);
    if ((s <= start && start < s + l) || (s > start && s <= start+ len))
    {
      nodes.push_back(std::get<0>(*l_it));
    }
    else if (s > start + len)
    {
      break;
    }
  }

  return nodes;
}
