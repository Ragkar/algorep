#pragma once

#include <vector>
#include <map>
#include <list>
#include <tuple>

#include "message.hh"

class Memory
{
public:
  Memory(int id, int n_nodes, const std::vector<int>& reachable);
  void kill_grid() const;

  // Single variable function
  int alloc(const int val);
  int read(const int v_id, bool& success) const;
  bool write(const int v_id, const int value) const;
  void free(const int v_id);

  // Multiple variable function
  int m_alloc(const int size);
  std::vector<int> m_read(const int v_id, const int start, const int len) const;
  bool m_write(const int v_id, const std::vector<int>& data, const int start) const;
  void m_free(const int v_id);

private:
  // Senders
  bool _ask_grant(const int i_dest, const int v_id = -1, const int rw = 0) const;
  void _ask_release(const int i_dest, const int v_id = -1) const;
  // Single value action
  bool _ask_s_alloc(const int i_dest, const int n_dst, const int v_id, const int val) const;
  bool _ask_s_read(const int i_dest, const int n_dst, const int v_id, int& val) const;
  bool _ask_s_write(const int i_dest, const int n_dst, const int v_id, const int val) const;
  void _ask_s_free(const int i_dest, const int n_dst, const int v_id) const;
  // Multiple value action
  bool _ask_m_alloc(const int i_dest, int n_dst, const int v_id, int size);
  std::vector<int> _ask_m_read(const int i_dest, const int v_id, const int start,
                               const int len ) const;
  bool _ask_m_write(const int i_dest, const int v_id, const std::vector<int>& data,
                    const int start) const;
  void _ask_m_free(const int i_dest, const int v_id);

  /*--  Misc ----------------------------------------------------------------*/
  // Get the next ID and node for allocation
  std::vector<int> _ask_last_id(const int i_dest) const;
  // Get node with the more free space
  int _ask_search(const int i_dest) const;
  // Get nodes containing part of an array needed.
  std::vector<int> _get_nodes(const int v_id, const int start, const int len) const;
  std::vector<int> _create_message(const int v_id = -1, const int tag = 0) const;
  void _send_to(const std::vector<int>& msg, const msg_t tag, const int i_dst) const;

  /*-- Attributes -----------------------------------------------------------*/
  int _id;
  int _n_nodes;
  /* List of reachable entry point on the grid */
  std::vector<int> _reachable;
  /* Map to find on which node it should read or write */
  std::map<int, int> _id_to_node;
  std::map<int, std::list<std::tuple<int, int, int>>> _id_to_nodes;
};
