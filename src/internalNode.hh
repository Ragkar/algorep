#pragma once

#include <list>
#include <map>
#include <tuple>
#include <queue>

#include "message.hh"
#include "utils.hh"

class InternalNode
{
public:
  InternalNode(const int id, const int p, bool running = true);
  void run();

private:
  // Senders
  void _send_quorum(const std::vector<int>& msg, const msg_t tag) const;
  void _send_to(const std::vector<int>& msg, const msg_t tag, const int i_dst) const;

  // Receivers
  void _wait_msg();
  // For lock management
  void _recv_failed(const int i_src, const std::vector<int>& msg);
  void _recv_grant(const int i_src, const std::vector<int>& msg);
  void _recv_inquire(const int i_src, const std::vector<int>& msg);
  void _recv_release(const int i_src, const std::vector<int>& msg);
  void _recv_require(const int i_src, const std::vector<int>& msg);
  void _recv_yield(const int i_src, const std::vector<int>& msg);
  // For user interface
  void _recv_kill(const int i_src, const std::vector<int>& msg);
  void _recv_last_id(const int i_src, const std::vector<int>& msg);
  void _recv_search(const int i_src, const std::vector<int>& msg);
  // Single data action
  void _recv_s_alloc(const int i_src, const std::vector<int>& msg);
  void _recv_s_read(const int i_src, const std::vector<int>& msg);
  void _recv_s_write(const int i_src, const std::vector<int>& msg);
  void _recv_s_free(const int i_src, const std::vector<int>& msg);
  // Multiple data action
  void _recv_m_alloc(const int i_src, const std::vector<int>& msg);
  void _recv_m_read(const int i_src, const std::vector<int>& msg);
  void _recv_m_write(const int i_src, const std::vector<int>& msg);
  void _recv_m_free(const int i_src, const std::vector<int>& msg);

  // Misc
  bool _is_internalNode(const int i_src);
  int _get_intermediate(const int i_dst);

  /*-- Attributes -----------------------------------------------------------*/
  int _id;
  int _row;
  int _col;
  int _p;
  bool _running;

  // Information to elected next ID and allocation node.
  int _less_filled;
  int _less_filled_size;
  int _last_id;
  int _counter;
  /* Map of counter for grant and fail msg receive for a lock */
  std::map<int, std::tuple<int, int>> _grant;

  /* Map of queue for node waiting a particular information */
  std::map<int, std::queue<int>> _inquire;
  std::map<int, std::queue<std::tuple<int, timestamp, int>>> _yield;

  // Memory
  unsigned _r_size;
  std::map<int, std::tuple<int, timestamp>> _s_memory;
  std::map<int, std::tuple<int, std::vector<int>, timestamp>> _m_memory;

  /* Map of lock: key is the ID of the var locked, the tuple contains the ID
   * of the node owning the lock and its timestamp */
  std::map<int, std::list<std::tuple<int, timestamp, int>>> _locks;
};
