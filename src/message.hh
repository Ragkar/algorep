#pragma once

#include <type_traits>

#define MAX_SIZE (100)

/* Basic msg:
 *    0 - 4: user timestamp
 *    4    : user ID
 *    5    : variable ID
 *    6    : tag
 *    7    : data
 *    ...
 *
 * Single value msg:
 *    0 - 4: user timestamp
 *    4    : user ID
 *    5    : variable ID
 *    6    : tag
 *    7    : internal node link to user
 *    8    : internal node dst of user
 *    ...
 *
 * Single value msg:
 *    0 - 4: user timestamp
 *    4    : user ID
 *    5    : variable ID
 *    6    : tag
 *    7    : internal node link to user
 *    8    : internal node dst of user
 *    9    : idx start in array
 *    10   : idx len in array
 *    ...
 */

/* FAIL tag   : 0: action in the past, 1: already lock.
 * REQUIRE tag: 0: write, 1: read
 * YIELD tag  : 0: FAIL, 1: SUCCESS
 * LAST_ID tag: 0: search, 1: return, 2: update
 *        data: - last id
 *              - r_size
 * S_READ tag : 0: not found, 1: found
 * M_ALLOC tag: 0: FAIL, 1: SUCCESS
 */

#define CRT_IDX (4)
#define ID_IDX  (5)
#define TAG_IDX (6)
#define DATA_IDX (7)

#define N_SRC (7)
#define N_DST (8)

#define M_START (9)
#define M_LEN (10)
#define M_DATA (11)

enum class msg_t
{
  // For Maekawa
  FAIL,
  GRANT,
  INQUIRE,
  RELEASE,
  REQUIRE,
  YIELD,
  // For memory management
  LAST_ID,
  KILL,
  SEARCH,
  // Single variable management
  S_ALLOC,
  S_READ,
  S_WRITE,
  S_FREE,
  // Multiple variable management
  M_ALLOC,
  M_READ,
  M_WRITE,
  M_FREE,
};

template <typename E>
constexpr typename std::underlying_type<E>::type underlying(E e) noexcept
{
  return static_cast<typename std::underlying_type<E>::type>(e);
}
