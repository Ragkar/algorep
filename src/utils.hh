#pragma once

#include <sys/time.h>
#include <vector>
#include <climits>

#define BITSIZE(Type) (sizeof(Type) * 8)

using timestamp = struct timeval;

inline timestamp to_timestamp(const std::vector<int>& ints)
{
    return { ((long)ints[0] << BITSIZE(int)) | (ints[1] & INT_MAX),
             ((long)ints[2] << BITSIZE(int)) | (ints[3] & INT_MAX) };
}

inline std::vector<int> to_int(const timestamp& ts)
{
    return { (int)(ts.tv_sec >> BITSIZE(int)),  (int)(ts.tv_sec & INT_MAX),
             (int)(ts.tv_usec >> BITSIZE(int)), (int)(ts.tv_usec & INT_MAX) };
}

bool operator<(const timestamp& ts1, const timestamp& ts2);
